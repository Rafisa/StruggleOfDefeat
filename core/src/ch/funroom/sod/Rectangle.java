package ch.funroom.sod;

public class Rectangle {
    public int x, y, width, height;

    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return "Rectangle { " +
            "pos=[" + x + ", " + y +
            "] size=[" + width +
            ", " + height +
            "] }";
    }
}
