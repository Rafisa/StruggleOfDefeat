package ch.funroom.sod;

import ch.funroom.sod.game_state.Connection;
import org.msgpack.core.MessagePack;
import org.msgpack.core.MessagePacker;
import org.msgpack.core.MessageUnpacker;
import org.msgpack.value.Value;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Manages network connection of Clients to the Server
 */
public class NetworkManager extends Thread {
    public static final int port = 1917;
    public ConcurrentLinkedQueue<Connection> connections;

    public NetworkManager(ConcurrentLinkedQueue<Connection> connections) {
        this.connections = connections;
    }

    @Override
    public void run() {
        java.net.ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (serverSocket != null) {
            java.net.Socket socket = null;
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                System.out.println("a connection Failed");
            }
            if (socket != null) {
                connect(socket);
            }
        }
    }

    private void connect(Socket socket) {
        //initialise variables
        ConcurrentLinkedQueue<IncomingMsg> inputs = new ConcurrentLinkedQueue<>();
        ConcurrentLinkedQueue<MessageWriter> outputs = new ConcurrentLinkedQueue<>();

        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
        } catch (Exception e) {

        }

        if (inputStream != null && outputStream != null) {
            connections.add(new Connection(inputs, outputs));
            new Reader(inputs, MessagePack.newDefaultUnpacker(inputStream)).start();
            new Writer(outputs, MessagePack.newDefaultPacker(outputStream)).start();
        }
    }

    private class Reader extends Thread {
        ConcurrentLinkedQueue<IncomingMsg> inputs;
        MessageUnpacker unpacker;

        public Reader(ConcurrentLinkedQueue<IncomingMsg> inputs, MessageUnpacker unpacker) {
            this.inputs = inputs;
            this.unpacker = unpacker;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    if (unpacker.unpackArrayHeader() == 2) {
                        IncomingMsg msg = new IncomingMsg();
                        msg.type = unpacker.unpackInt();
                        msg.value = unpacker.unpackValue();
                        inputs.add(msg);
                    }
                } catch (Exception e) {
                    break;
                }
            }
        }
    }

    private class Writer extends Thread {
        ConcurrentLinkedQueue<MessageWriter> outputs;
        MessagePacker packer;

        public Writer(ConcurrentLinkedQueue<MessageWriter> outputs, MessagePacker packer) {
            this.outputs = outputs;
            this.packer = packer;
        }

        @Override
        public void run() {
            while (true) {
                MessageWriter next = outputs.poll();
                if (next != null) {
                    try {
                        next.write(packer);
                        packer.flush();
                    } catch (IOException e) {
                        break;
                    }
                }
            }
        }
    }
}

