package ch.funroom.sod;

import ch.funroom.sod.game_state.Behaviour;
import ch.funroom.sod.game_state.DataConnector;
import ch.funroom.sod.game_state.GameObject;
import ch.funroom.sod.game_state.PhysicalConnector;
import ch.funroom.sod.game_state.behaviours.Cockpit;
import ch.funroom.sod.game_state.behaviours.Engine;
import ch.funroom.sod.game_state.behaviours.Gun;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import java.util.ArrayList;

public class Part {
    public byte behaviour;
    public ArrayList<Rectangle> rectangles;
    public ArrayList<Vector2> dataConnectors;
    public ArrayList<Pair<Vector2, Float>> physicalConnectors;

    public Behaviour loadBehaviour(int connection) {
        switch (behaviour) {
            case 0:
                return new Cockpit(connection);
            case 1:
                return new Engine(size());
            case 2:
                return new Gun();
            default:
                return null;
        }
    }

    public Vector2 size() {
        float width = 0, height = 0;
        for (Rectangle rectangle : rectangles) {
            float x = rectangle.x + rectangle.width;
            float y = rectangle.y + rectangle.height;
            if (x > width) width = x;
            if (y > height) height = y;
        }
        return new Vector2(width, height);
    }

    public Body loadBody(World world, boolean mirror, Vector2 pos, float ang) {
        Vector2 size = size();

        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.DynamicBody;
        def.position.set(pos);
        def.angle = ang;
        def.fixedRotation = false;
        Body body = world.createBody(def);

        for (Rectangle rectangle : rectangles) {
            FixtureDef fDef = new FixtureDef();
            PolygonShape shape = new PolygonShape();
            float x = rectangle.x + rectangle.width / 2f - size.x / 2f;
            float y = rectangle.y + rectangle.height / 2f - size.y / 2f;
            if (mirror) x = -x;
            shape.setAsBox(rectangle.width / 2f, rectangle.height / 2f, new Vector2(x, y), 0);
            fDef.shape = shape;
            fDef.density = 2.5f;
            fDef.restitution = 0f;
            body.createFixture(fDef);
        }

        return body;
    }

    public ArrayList<DataConnector> loadDataConnectors(boolean mirror) {
        ArrayList<DataConnector> loadedConnectors = new ArrayList<>(dataConnectors.size());
        for (Vector2 pos: dataConnectors) {
            Vector2 newPos = new Vector2(pos);
            if (mirror) newPos.x = -newPos.x;
            loadedConnectors.add(new DataConnector(newPos));
        }
        return loadedConnectors;
    }

    public ArrayList<PhysicalConnector> loadPhysicalConnectors(Body body, boolean mirror) {
        ArrayList<PhysicalConnector> loadedConnectors = new ArrayList<>(physicalConnectors.size());
        for (Pair<Vector2, Float> connector: physicalConnectors) {
            Vector2 pos = new Vector2(connector.t);
            if (mirror) pos.x = -pos.x;
            loadedConnectors.add(new PhysicalConnector(body, pos, connector.u));
        }
        return loadedConnectors;
    }

    @Override
    public String toString() {
        return "SpawnPart{" +
            "rectangles=" + rectangles +
            ", dataConnectors=" + dataConnectors +
            ", physicalConnectors=" + physicalConnectors +
            '}';
    }
}
