package ch.funroom.sod;

/**
 * Because f***in Java has no Tuples!!!
 *
 * @param <T> the first Type
 * @param <U> the second Type
 */
public class Pair<T, U> {
    public T t;
    public U u;

    public Pair(T t, U u) {
        this.t = t;
        this.u = u;
    }

    @Override
    public String toString() {
        return "(" + t + ", " + u + ')';
    }
}
