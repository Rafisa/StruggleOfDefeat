package ch.funroom.sod;

import ch.funroom.sod.game_state.Connection;
import ch.funroom.sod.game_state.GameObject;
import ch.funroom.sod.game_state.GameState;
import ch.funroom.sod.game_state.behaviours.Engine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJoint;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import org.msgpack.core.MessagePack;
import org.msgpack.core.MessagePacker;
import org.msgpack.core.MessageUnpacker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * The first class initialised in the core module.
 */
public class StruggleOfDefeat extends com.badlogic.gdx.Game {
    public static final int teams = 1;
    public Variant variant;
    public float spawnTimer;
    public ArrayList<Part> parts;

    /**
     * Initializes the StruggleOfDefeat.
     */
    @Override
    public void create() {
        spawnTimer = 240;
        variant = new Spawn();
        parts = new ArrayList<>();

        try {
            File file = new File("parts.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                MessageUnpacker unpacker = MessagePack.newDefaultUnpacker(new FileInputStream(line));
                parts.add(Read.readPart(unpacker.unpackValue()));
                unpacker.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        System.out.println(parts.size());
    }

    /**
     * This is the update cycle of the game witch is called for every tick uses the render method of game.
     */
    @Override
    public void render() {
        variant.render();

        if (spawnTimer > 0) {
            spawnTimer -= Gdx.graphics.getDeltaTime();
        }

        if (variant.getClass() == Spawn.class && (((Spawn)variant).update() || spawnTimer <= 0)) {
            Spawn spawn = ((Spawn) variant);
            variant = new Game(spawn.connections);
            spawn.load(parts, ((Game) variant).gameState);
        }
    }

    @Override
    public void resize(int width, int height) {
        variant.resize(width, height);
    }

    public interface Variant {
        void render();

        void resize(int width, int height);
    }

    public class Spawn implements Variant {
        public ConcurrentLinkedQueue<Connection> newConnections = new ConcurrentLinkedQueue<>();
        public ArrayList<Connection> connections;
        public SpawnGrid[] grids;
        public int currentTeam;

        public Spawn() {
            currentTeam = 0;
            newConnections = new ConcurrentLinkedQueue<>();
            new NetworkManager(newConnections).start();
            connections = new ArrayList<>();
            grids = new SpawnGrid[teams];
            for (int i = 0; i < grids.length; i++) {
                grids[i] = new SpawnGrid(64, 64);
            }
        }

        public void load(ArrayList<Part> parts, GameState gameState) {
            for (int i = 0; i < grids.length; i++) {
                Vector2 pos;
                float ang;
                if (i % 2 == 0) {
                    pos = new Vector2(i / 2 * 128, -164);
                    ang = 0;
                } else {
                    pos = new Vector2(i / 2 * 128, 164);
                    ang = (float)Math.PI;
                }
                grids[i].load(parts, gameState, pos, ang);
            }

            ArrayList<GameObject> gameObjects = gameState.gameObjects;

            for (int i = 0; i < gameObjects.size(); i++) {
                for (int j = i + 1; j < gameObjects.size(); j++) {
                    // get objects
                    GameObject objI = gameObjects.get(i);
                    GameObject objJ = gameObjects.get(j);

                    if (parts.size() > objI.type && parts.size() > objJ.type) {
                        // iterate through physical connections of the objects i and j.
                        for (int ci = 0; ci < parts.get(objI.type).physicalConnectors.size(); ci++) {
                            for (int cj = 0; cj < parts.get(objJ.type).physicalConnectors.size(); cj++) {
                                // get connector positions
                                Vector2 connectorPosI = new Vector2(parts.get(objI.type).physicalConnectors.get(ci).t);
                                Vector2 connectorPosJ = new Vector2(parts.get(objJ.type).physicalConnectors.get(cj).t);
                                if (objI.mirror) connectorPosI.x = -connectorPosI.x;
                                if (objJ.mirror) connectorPosJ.x = -connectorPosJ.x;

                                // calculate distance between connector points
                                Vector2 positionI = objI.body.getWorldPoint(connectorPosI);
                                Vector2 positionJ = objJ.body.getWorldPoint(connectorPosJ);
                                float distance = new Vector2(positionJ).sub(positionI).len();

                                if (distance <= 0.1) {
                                    // add a joint between the objects
                                    WeldJointDef jointDef = new WeldJointDef();
                                    jointDef.initialize(
                                        objI.body,
                                        objJ.body,
                                        new Vector2(positionI).add(positionJ).scl(0.5f)
                                    );
                                    WeldJoint joint = (WeldJoint)gameState.world.createJoint(jointDef);
                                    gameState.joints.add(joint);
                                }
                            }
                        }
                    }
                }
            }
        }

        public boolean update() {
            for (Connection connection = newConnections.poll(); connection != null; connection = newConnections.poll())
            {
                grids[currentTeam].teamMembers.add(new Pair<>(connections.size(), false));
                connections.add(connection);
                grids[currentTeam].sendSize(connection);
                currentTeam = (currentTeam + 1) % grids.length;
                System.out.println("new connection");
            }

            boolean ready = true;
            for (SpawnGrid grid : grids) {
                ready = grid.update(parts, connections) && ready;
            }

            return ready;
        }

        @Override
        public void render() {

        }

        @Override
        public void resize(int width, int height) {

        }
    }

    public class Game implements Variant {
        public GameState gameState;
        public Debug debug;

        public Game(ArrayList<Connection> connections) {
            for (Connection connection: connections) {
                connection.sendMessage((MessagePacker packer) -> {
                    Write.writeMsgHeader(packer, 1, 0);
                });
            }
            gameState = new GameState(connections);
            debug = new Debug();
        }

        @Override
        public void render() {
            gameState.update(Gdx.graphics.getDeltaTime());
            debug.render(Gdx.graphics.getDeltaTime(), gameState);
        }

        @Override
        public void resize(int width, int height) {
            debug.resize(width, height);
        }
    }
}
