package ch.funroom.sod.game_state;

import ch.funroom.sod.Part;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import java.util.ArrayList;

/**
 * A GameObject is a abstraction of a physical behaviours.
 * It has a physical body,
 * a flag to remove it from the gameState,
 * a deformation value (used to represent how broken a behaviours is(gets deformed by physical impacts with other
 * body's.)) and it has a heat value which gets transferee to other behaviours with the heatConductivity as parameter
 * for how fast.
 * A GameObject can have connector fields and can return them by index
 */
public class GameObject {
    public byte type;
    public Body body;
    public Vector2 size;
    public boolean mirror;
    public ArrayList<DataConnector> dataConnectors;
    public boolean remove;
    public float deformation;
    public float forceThreshold;
    public int heat, heatConductivity;
    public Behaviour behaviour;

    public static GameObject loadGameObject(
        ArrayList<Part> parts,
        GameState gameState,
        byte type,
        boolean mirror,
        int connection,
        Vector2 pos,
        float ang
    ) {
        if (parts.size() > type) {
            return new GameObject(
                gameState,
                type,
                parts.get(type),
                mirror,
                parts.get(type).loadBehaviour(connection),
                pos,
                ang
            );
        }
        return null;
    }

    public GameObject(
        GameState gameState,
        byte type,
        Part part,
        boolean mirror,
        Behaviour behaviour,
        Vector2 pos,
        float ang
    ) {
        this.type = type;
        this.mirror = mirror;
        this.behaviour = behaviour;

        size = part.size();
        body = part.loadBody(gameState.world, mirror, pos, ang);
        body.setBullet(false);
        dataConnectors = part.loadDataConnectors(mirror);
    }

    public GameObject(
        byte type,
        Body body,
        Vector2 size,
        Behaviour behaviour
    ) {
        this.type = type;
        this.body = body;
        this.size = size;
        this.behaviour = behaviour;

        mirror = false;
        dataConnectors = new ArrayList<>();
        remove = false;
        deformation = 0;
        forceThreshold = 1000;
        heat = 0;
        heatConductivity = 0;
    }
}
