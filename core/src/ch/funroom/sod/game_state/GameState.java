package ch.funroom.sod.game_state;

import ch.funroom.sod.game_state.behaviours.None;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.sun.jmx.remote.internal.ArrayQueue;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * the state of the game.
 */
public class GameState {
    public static final float contactRange = 0.1f;

    public ArrayList<Connection> connections;
    public World world;
    public ArrayList<GameObject> gameObjects;
    public ArrayQueue<GameObject> addQueue;
    public ArrayList<Joint> joints;
    public CollisionManager collisionManager;

    /**
     * initialises variables and sets variables
     */
    public GameState(ArrayList<Connection> connections) {
        this.connections = connections;
        world = new World(new Vector2(0, 0), true);
        world.setContactListener(collisionManager);
        gameObjects = new ArrayList<>();
        addQueue = new ArrayQueue<>(256);
        joints = new ArrayList<>();
        collisionManager = new CollisionManager();

        int xSize = 100;
        int ySize = 100;
        for (int x = -xSize / 4; x < xSize / 4; x++) {
            for (int y = -ySize / 4; y < ySize / 4; y++) {
                if (MathUtils.random() > 0.98f) {
                    float radius = MathUtils.random(0.5f, 2.8f);
                    GameObject asteroid = new GameObject(
                        (byte)4,
                        createCircle(radius, new Vector2(x * 4, y * 4), 5, 5),
                        new Vector2(radius * 2, radius * 2),
                        new None()
                    );
                    gameObjects.add(asteroid);
                }
            }
        }
    }

    /**
     * The update cycle of the game. Updates all GameObjects of the gameState and removes them if the remove flag is
     * true. Enforces Connections.
     * Updates the world and the collisionManager
     *
     * @param delta time passed between cycle
     */
    public void update(float delta) {
        gameObjects.addAll(addQueue);
        addQueue.clear();
        enforceConnections();
        world.step(delta, 16, 8);
        collisionManager.update();
        Iterator<GameObject> iter = gameObjects.iterator();
        while (iter.hasNext()) {
            GameObject gameObject = iter.next();

            for (DataConnector connector : gameObject.dataConnectors) {
                connector.output = 0;
            }

            if (gameObject.remove) {
                world.destroyBody(gameObject.body);
                iter.remove();
            } else {
                gameObject.behaviour.update(gameObject, this);
            }

            for (DataConnector connector : gameObject.dataConnectors) {
                connector.input = 0;
            }
        }
    }

    /**
     * Pulls physicalConnectors together if in range.
     */
    private void enforceConnections() {
        for (int i = 0; i < gameObjects.size(); i++) {
            for (int j = i + 1; j < gameObjects.size(); j++) {

                // transmit data
                for (int ci = 0; ci < gameObjects.get(i).dataConnectors.size(); ci++) {
                    for (int cj = 0; cj < gameObjects.get(j).dataConnectors.size(); cj++) {
                        GameObject objI = gameObjects.get(i);
                        GameObject objJ = gameObjects.get(j);

                        DataConnector connectorI = objI.dataConnectors.get(ci);
                        DataConnector connectorJ = objJ.dataConnectors.get(cj);

                        if (inRange(objI.body.getWorldPoint(connectorI.pos),
                                    objJ.body.getWorldPoint(connectorJ.pos),
                                    contactRange))
                        {
                            connectorI.input = connectorJ.output;
                            connectorJ.input = connectorI.output;
                        }
                    }
                }
            }
        }
    }

    /**
     * Checks if 2 position are within the given range and returns true if yes.
     *
     * @param pos0  the first position
     * @param pos1  the second position
     * @param range the range.
     * @return whether the positions are in range.
     */
    private boolean inRange(Vector2 pos0, Vector2 pos1, float range) {
        float distance = new Vector2(pos1).sub(pos0).len();
        if (distance <= range) {
            return true;
        }
        return false;
    }

    /**
     * creates a body of the shape of a box
     *
     * @param width   width of to be created body
     * @param height  height of to be created body
     * @param pos     Position of to be created body
     * @param ang     The ang
     * @param density how "heavy" the behaviours is (for calculation of slowing)
     * @return the created body
     */
    public Body createBox(float width, float height, Vector2 pos, float ang, float density) {
        Body body;
        BodyDef def = new BodyDef();
        FixtureDef fDef = new FixtureDef();

        def.type = BodyDef.BodyType.DynamicBody;
        def.position.set(pos);
        def.angle = ang;
        def.fixedRotation = false;

        body = world.createBody(def);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width / 2, height / 2);
        fDef.shape = shape;

        fDef.density = density;
        fDef.restitution = 0f;

        body.createFixture(fDef);
        shape.dispose();

        return body;
    }

    /**
     * creates a body of the shape of a circle
     *
     * @param radius  the radius of the circle
     * @param pos     Position of to be created body
     * @param ang     The ang
     * @param density how "heavy" the behaviours is (for calculation of slowing)
     * @return the created body
     */
    public Body createCircle(float radius, Vector2 pos, float ang, float density) {
        Body body;
        BodyDef def = new BodyDef();
        FixtureDef fDef = new FixtureDef();

        def.type = BodyDef.BodyType.DynamicBody;
        def.position.set(pos);
        def.fixedRotation = false;

        body = world.createBody(def);

        CircleShape shape = new CircleShape();
        shape.setRadius(radius);
        fDef.shape = shape;

        fDef.density = density;
        fDef.restitution = 0f;

        body.createFixture(fDef);

        shape.dispose();

        return body;
    }
}