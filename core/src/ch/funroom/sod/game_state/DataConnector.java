package ch.funroom.sod.game_state;

import com.badlogic.gdx.math.Vector2;

public class DataConnector {
    // the position of the connector relative to the containing object
    public Vector2 pos;

    // the input and output that are exchanged with other physicalConnectors on connection
    public int input, output;

    public DataConnector(Vector2 pos) {
        this.pos = pos;
        this.input = 0;
        this.output = 0;
    }
}
