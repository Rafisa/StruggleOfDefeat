package ch.funroom.sod.game_state;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;

/**
 * Used to connect gameObjects together. Has a position relative to the origin of a body. Wen two physicalConnectors are
 * close (range defined by the gameState) input field will be set to the value of the output field of the other
 * connector. Has a strength that determines the attraction force of the connector.
 */
public class PhysicalConnector {
    public Vector2 pos;// position relative to the containing object
    public float strength;
    public DistanceJoint joint;

    public PhysicalConnector(Body body, Vector2 pos, float strength) {
        this.pos = pos;
        this.strength = strength;

        joint = null;

//        MouseJointDef jointDef = new MouseJointDef();
//        jointDef.bodyA = body;
//        jointDef.bodyB = body;
    }
}
