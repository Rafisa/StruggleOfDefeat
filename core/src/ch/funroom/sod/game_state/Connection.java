package ch.funroom.sod.game_state;

import ch.funroom.sod.IncomingMsg;
import ch.funroom.sod.MessageWriter;
import org.msgpack.value.Value;

import java.util.concurrent.ConcurrentLinkedQueue;

public class Connection {
    ConcurrentLinkedQueue<IncomingMsg> inputs;
    ConcurrentLinkedQueue<MessageWriter> outputs;

    public Connection(ConcurrentLinkedQueue<IncomingMsg> inputs, ConcurrentLinkedQueue<MessageWriter> outputs) {
        this.inputs = inputs;
        this.outputs = outputs;
    }

    public IncomingMsg nextOutput() {
        return inputs.poll();
    }

    public void sendMessage(MessageWriter message) {
        outputs.add(message);
    }
}
