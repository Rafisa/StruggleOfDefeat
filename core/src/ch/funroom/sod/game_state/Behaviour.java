package ch.funroom.sod.game_state;

public interface Behaviour {
    void update(GameObject gameObject, GameState gameState);
}
