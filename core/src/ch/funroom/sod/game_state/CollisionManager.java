package ch.funroom.sod.game_state;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import java.util.ArrayList;
import java.util.Stack;

// TODO use actual physics for calculation and clean up code, merge this class with game state.
public class CollisionManager implements ContactListener {
    private ArrayList<Body[]> contacts;
    private ArrayList<Float> contactVelocityA, contactVelocityB;
    private ArrayList<Long> contactTimes;
    private Stack<PastContact> pastContactsStack;

    public CollisionManager() {
        contacts = new ArrayList<>();
        contactVelocityA = new ArrayList<>();
        contactVelocityB = new ArrayList<>();
        contactTimes = new ArrayList<>();
        pastContactsStack = new Stack<>();
    }

    public void update() {
        while (!pastContactsStack.isEmpty()) {
            pastContactsStack.pop().pastContactHit();
        }

    }

    @Override
    public void beginContact(Contact contact) {
        Body bodyA = contact.getFixtureA().getBody();
        Body bodyB = contact.getFixtureB().getBody();
//        ((GenericObject)bodyA.getUserData()).coliding = true;
//        ((GenericObject)bodyB.getUserData()).coliding = true;
        Body[] bodies = {bodyA, bodyB};
        contacts.add(bodies);
//        contactTimes.add(System.currentTimeMillis());
        contactVelocityA.add(bodyA.getLinearVelocity().len());
        contactVelocityB.add(bodyB.getLinearVelocity().len());


        Vector2 velocityvectorDifference = bodyA.getLinearVelocity().sub(bodyB.getLinearVelocity());

        float velocityDiffenrence = velocityvectorDifference.len();
//        float velocityDiffenrence = (float) Math.sqrt(Math.pow(velocityvectorDifference.x, 2) * Math.pow(velocityvectorDifference.x, 2));

        float forceOfColition = (bodyA.getLinearVelocity().scl(bodyA.getMass()).sub(bodyB.getLinearVelocity().scl(bodyB.getMass()))).len();
//        System.out.println("\n force of colition is = " + forceOfColition);


//        pastContactsStack.add(new PastContact(bodyB.getMass() *  velocityDiffenrence, (GenericObject)bodyA.getUserData(), ((GenericObject)bodyB.getUserData())));
//        pastContactsStack.add(new PastContact(bodyA.getMass() *  velocityDiffenrence, (GenericObject)bodyB.getUserData(), ((GenericObject)bodyA.getUserData())));

        pastContactsStack.add(new PastContact(forceOfColition, (GameObject) bodyA.getUserData(), ((GameObject) bodyB.getUserData())));
        pastContactsStack.add(new PastContact(forceOfColition, (GameObject) bodyB.getUserData(), ((GameObject) bodyA.getUserData())));


    }

    @Override
    public void endContact(Contact contact) {

//        Body bodyA = contact.getFixtureA().getBody();
//        Body bodyB = contact.getFixtureB().getBody();
//
//        int index = 0;
//        boolean found = false;
//
//        for(int i = 0; i < contacts.size(); i++){
//            Body[] gameObjects = contacts.get(i);
//            if(gameObjects[0] == bodyA && gameObjects[1] == bodyB || gameObjects[1] == bodyA && gameObjects[0] == bodyB){
//                index = i;
//                found = true;
//                break;
//            }
//        }
//
//        if(found){
//            float timeDifference = (System.currentTimeMillis() - contactTimes.get(index)) / 1000;
//            if(timeDifference <= 0){
//                timeDifference = Gdx.graphics.getDeltaTime();
//            }
//            float forceA = (contactVelocityA.get(index) - bodyA.getLinearVelocity().len()) / timeDifference * bodyA.getMass();
//            float forceB = (contactVelocityB.get(index) - bodyB.getLinearVelocity().len()) / timeDifference * bodyB.getMass();
//            float force = Math.max(forceA, forceB);
//
//            contacts.remove(index);
//            contactTimes.remove(index);
//            contactVelocityA.remove(index);
//            contactVelocityB.remove(index);
//            pastContactsStack.add(new PastContact(force, (GenericObject)bodyA.getUserData(), ((GenericObject)bodyB.getUserData())));
//            pastContactsStack.add(new PastContact(force, (GenericObject)bodyB.getUserData(), ((GenericObject)bodyA.getUserData())));
////            ((GenericObject)bodyA.getUserData()).hit("force", force, ((GenericObject)bodyB.getUserData()));
////            ((GenericObject)bodyB.getUserData()).hit("force", force, ((GenericObject)bodyA.getUserData()));
//
//            ((GenericObject)bodyA.getUserData()).coliding = false;
//            ((GenericObject)bodyB.getUserData()).coliding = false;
//        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        Body bodyA = contact.getFixtureA().getBody();
        Body bodyB = contact.getFixtureB().getBody();
    }

    private class PastContact {
        float pastContactForce;
        GameObject source, target;

        public PastContact(float pastContactForce, GameObject source, GameObject target) {

            this.pastContactForce = pastContactForce;
            this.source = source;
            this.target = target;
        }

        public void pastContactHit() {
            if (pastContactForce * 0.3f > source.forceThreshold) {
                source.deformation += pastContactForce * 0.3f;
            }
        }
    }
}
