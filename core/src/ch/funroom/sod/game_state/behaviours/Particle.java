package ch.funroom.sod.game_state.behaviours;

import ch.funroom.sod.game_state.Behaviour;
import ch.funroom.sod.game_state.GameObject;
import ch.funroom.sod.game_state.GameState;
import com.badlogic.gdx.Gdx;

public class Particle implements Behaviour {
    public float time;

    public Particle(float time) {
        this.time = time;
    }

    @Override
    public void update(GameObject gameObject, GameState gameState) {
        time -= Gdx.graphics.getDeltaTime();
        if (time <= 0) {
            gameObject.remove = true;
        }
    }
}
