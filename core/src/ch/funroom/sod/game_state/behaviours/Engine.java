package ch.funroom.sod.game_state.behaviours;

import ch.funroom.sod.game_state.Behaviour;
import ch.funroom.sod.game_state.DataConnector;
import ch.funroom.sod.game_state.GameObject;
import ch.funroom.sod.game_state.GameState;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Engine implements Behaviour {
    public static final int maxParticlesPerSecond = 128;
    public static final float particleRadius = 0.1f;
    public static final float impulseMultiplier = 8;
    public float spawnDistance, spawnRange;
    public float particles;

    public Engine(Vector2 size) {
        particles = 0;
        spawnDistance = -(size.y / 2 + particleRadius);
        spawnRange = size.x / 2;
    }

    @Override
    public void update(GameObject gameObject, GameState gameState) {
        int particlesPerSecond = 0;

        for (DataConnector dataConnector: gameObject.dataConnectors) {
            particlesPerSecond = Math.max(Math.min(dataConnector.input, maxParticlesPerSecond), particlesPerSecond);
        }

        int particleCount = (int) (spawnRange / (particleRadius * 2));
        float particleDistance = spawnRange / particleCount;

        particles += particlesPerSecond * Gdx.graphics.getDeltaTime();

        int particlesToSpawn = Math.min((int)particles, particleCount);

        for (int i = 0; i < particlesToSpawn; i++) {
            Vector2 point0 = new Vector2(i * particleDistance, spawnDistance);
            Vector2 point1 = new Vector2(-i * particleDistance, spawnDistance);

            point0.rotateRad(gameObject.body.getAngle());
            point1.rotateRad(gameObject.body.getAngle());

            point0.add(gameObject.body.getPosition());
            point1.add(gameObject.body.getPosition());

            spawnParticle(gameState, gameObject, point0);
            spawnParticle(gameState, gameObject, point1);
        }
        particles -= particlesToSpawn;
    }

    private void spawnParticle(GameState gameState, GameObject gameObject, Vector2 pos) {
        GameObject particle = new GameObject(
            (byte) 5,
            gameState.createCircle(particleRadius, pos, 0, 8),
            new Vector2(particleRadius * 2, particleRadius * 2),
            new Particle(0.5f)
        );

        particle.body.setLinearVelocity(gameObject.body.getLinearVelocityFromWorldPoint(pos));

        particle.heat = 2000;
        particle.heatConductivity = Integer.MAX_VALUE / 2;

        Vector2 impulse = new Vector2(0, -impulseMultiplier).rotateRad(gameObject.body.getAngle());
//        impulse.scl(MathUtils.random(0.9f, 1.1f));

        particle.body.applyLinearImpulse(
            impulse,
            particle.body.getPosition(),
            true
        );

        gameObject.body.applyLinearImpulse(
            impulse.scl(-1),
            gameObject.body.getPosition(),
            true
        );
        gameState.addQueue.add(particle);
    }
}
