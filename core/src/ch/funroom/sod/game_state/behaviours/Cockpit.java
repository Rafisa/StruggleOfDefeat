package ch.funroom.sod.game_state.behaviours;


import ch.funroom.sod.*;
import ch.funroom.sod.game_state.*;
import ch.funroom.sod.game_state.behaviours.cockpit.ConnectorController;
import ch.funroom.sod.game_state.behaviours.cockpit.GameObjectSensor;
import org.msgpack.core.MessagePacker;
import org.msgpack.value.ArrayValue;
import org.msgpack.value.Value;

import java.io.IOException;

public class Cockpit implements Behaviour {
    public int connection;
    public GameObjectSensor sensor;
    public ConnectorController controller;

    public Cockpit(int connection) {
        this.connection = connection;
        sensor = new GameObjectSensor(128f);
        controller = new ConnectorController();
    }

    @Override
    public void update(GameObject gameObject, GameState gameState) {
        sensor.update(gameState, gameObject, this);
        controller.update(gameObject);
        if (gameState.connections.size() > this.connection) {
            Connection connection = gameState.connections.get(this.connection);
            for (MessageWriter output = sensor.nextOutput(); output != null; output = sensor.nextOutput()) {
                connection.sendMessage(new ShipHeader(0, output));
            }

            for (MessageWriter output = controller.nextOutput(); output != null; output = controller.nextOutput()) {
                connection.sendMessage(new ShipHeader(1, output));
            }
            for (IncomingMsg msg = connection.nextOutput(); msg != null; msg = connection.nextOutput()) {
                if (msg.type == 3) {
                    if (msg.value.isArrayValue() && msg.value.asArrayValue().get(0).isArrayValue()) {
                        ArrayValue value = msg.value.asArrayValue().get(0).asArrayValue();
                        if (value.size() == 2 && value.get(0).isIntegerValue() && value.get(1).isArrayValue()) {
                            int i = value.get(0).asIntegerValue().asInt();
                            ArrayValue content = value.get(1).asArrayValue();
                            switch (i) {
                                case 0:
                                    sensor.sendMessage(content);
                                    break;
                                case 1:
                                    controller.sendMessage(content);
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    public class ShipHeader implements MessageWriter {
        public int device;
        public MessageWriter writer;

        public ShipHeader(int device, MessageWriter writer) {
            this.device = device;
            this.writer = writer;
        }

        @Override
        public void write(MessagePacker packer) throws IOException {
            Write.writeMsgHeader(packer, 3, 1);
            packer.packArrayHeader(2);
            packer.packInt(device);
            writer.write(packer);
        }
    }
}
