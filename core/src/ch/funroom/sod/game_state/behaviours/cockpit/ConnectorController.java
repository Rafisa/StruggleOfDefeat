package ch.funroom.sod.game_state.behaviours.cockpit;

import ch.funroom.sod.MessageWriter;
import ch.funroom.sod.Pair;
import ch.funroom.sod.game_state.GameObject;
import org.msgpack.core.MessagePacker;
import org.msgpack.value.ArrayValue;
import org.msgpack.value.Value;

import java.util.ArrayList;

public class ConnectorController {
    ArrayList<MessageWriter> outputs;
    ArrayList<Pair<Integer, Integer>> connectors;

    public ConnectorController() {
        connectors = new ArrayList<>();
        outputs = new ArrayList<>();
    }

    public MessageWriter nextOutput() {
        if (outputs.size() > 0) {
            MessageWriter message = outputs.get(0);
            outputs.remove(0);
            return message;
        }
        return null;
    }

    public void sendMessage(ArrayValue message) {
        if (message.size() == 2 && message.get(0).isIntegerValue() && message.get(1).isIntegerValue()) {
            int i = message.get(0).asIntegerValue().asInt();
            if (i < connectors.size()) {
                connectors.get(i).u = message.get(1).asIntegerValue().asInt();
            }
        }
    }

    public void update(GameObject gameObject) {
        while (connectors.size() < gameObject.dataConnectors.size()) {
            connectors.add(new Pair<>(0, 0));
        }
        for (int i = 0; i < connectors.size() && i < gameObject.dataConnectors.size(); i++) {
            // sync input
            if (connectors.get(i).t != gameObject.dataConnectors.get(i).input) {
                connectors.get(i).t = gameObject.dataConnectors.get(i).input;
                int index = i;
                int input = gameObject.dataConnectors.get(i).input;
                outputs.add((MessagePacker packer) -> {
                    packer.packArrayHeader(2);
                    packer.packInt(index);
                    packer.packInt(input);
                });
            }

            // sync output
            gameObject.dataConnectors.get(i).output = connectors.get(i).u;
        }
    }
}
