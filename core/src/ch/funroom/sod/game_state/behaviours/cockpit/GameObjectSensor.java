package ch.funroom.sod.game_state.behaviours.cockpit;

import ch.funroom.sod.MessageWriter;
import ch.funroom.sod.game_state.GameObject;
import ch.funroom.sod.game_state.GameState;
import ch.funroom.sod.game_state.behaviours.Cockpit;
import com.badlogic.gdx.math.Vector2;
import org.msgpack.core.MessagePacker;
import org.msgpack.value.ArrayValue;
import org.msgpack.value.Value;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class GameObjectSensor {
    public float range;
    public boolean send;
    public float ang, angVel;
    public ArrayList<GameObjectData> visible;

    public GameObjectSensor(Float range) {
        this.range = range;
        send = true;
        ang = 0;
        angVel = 0;
        visible = new ArrayList<>();
    }

    public MessageWriter nextOutput() {
        if (send) {
            send = false;
            float ang = this.ang;
            float angVel = this.angVel;
            ArrayList<GameObjectData> visible = this.visible;
            this.visible = new ArrayList<>();
            return (MessagePacker packer) -> {
                packer.packArrayHeader(3);
                packer.packFloat(ang);
                packer.packFloat(angVel);
                packer.packArrayHeader(visible.size());
                for (GameObjectData gameObject : visible) {
                    gameObject.write(packer);
                }
            };
        }
        return null;
    }

    public void sendMessage(ArrayValue message) {
        if (message.size() == 1 && message.get(0).isBooleanValue()) {
            send = message.get(0).asBooleanValue().getBoolean();
        }
    }

    public void update(GameState gameState, GameObject gameObject, Cockpit cockpit) {
        if (!gameObject.remove) {
            ang = gameObject.body.getAngle();
            angVel = gameObject.body.getAngularVelocity();

            visible.clear();
            Iterator<GameObject> iter = gameState.gameObjects.iterator();
            while (iter.hasNext()) {
                GameObject next = iter.next();
                if (inRange(gameObject, next)) {
                    visible.add(new GameObjectData(gameObject, next));
                }
            }
        }
        send = true;
    }

    private boolean inRange(GameObject object0, GameObject object1) {
        float distance = new Vector2(object1.body.getPosition()).sub(object0.body.getPosition()).len();

        if (distance <= range) {
            return true;
        } else {
            return false;
        }
    }

    public class GameObjectData implements MessageWriter {
        public byte objType;
        public boolean mirror;
        public float sizeX, sizeY;
        public float posX, posY;
        public float velX, velY;
        public float ang;
        public float angVel;

        public GameObjectData(GameObject cockpit, GameObject gameObject) {
            objType = gameObject.type;

            mirror = gameObject.mirror;

            sizeX = gameObject.size.x;
            sizeY = gameObject.size.y;

            posX = gameObject.body.getPosition().x - cockpit.body.getPosition().x;
            posY = gameObject.body.getPosition().y - cockpit.body.getPosition().y;

            velX = gameObject.body.getLinearVelocity().x - cockpit.body.getLinearVelocity().x;
            velY = gameObject.body.getLinearVelocity().y - cockpit.body.getLinearVelocity().y;

            ang = gameObject.body.getAngle();

            angVel = gameObject.body.getAngularVelocity();
        }

        @Override
        public void write(MessagePacker packer) throws IOException {
            packer.packArrayHeader(7);
            packer.packByte(objType);
            packer.packBoolean(mirror);
            write2dVec(packer, sizeX, sizeY);
            write2dVec(packer, posX, posY);
            write2dVec(packer, velX, velY);
            packer.packFloat(ang);
            packer.packFloat(angVel);
        }
    }

    public static void write2dVec(MessagePacker packer, float x, float y) throws IOException {
        packer.packArrayHeader(2);
        packer.packFloat(x);
        packer.packFloat(y);
    }
}
