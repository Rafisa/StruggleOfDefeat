package ch.funroom.sod.game_state.behaviours;

import ch.funroom.sod.game_state.Behaviour;
import ch.funroom.sod.game_state.GameObject;
import ch.funroom.sod.game_state.GameState;

public class None implements Behaviour {
    @Override
    public void update(GameObject gameObject, GameState gameState) {
        // absolutely nothing
    }
}
