package ch.funroom.sod.game_state.behaviours;

import ch.funroom.sod.game_state.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Gun implements Behaviour {
    public int maxShotsPerSecond;
    public float coolDown;

    public Gun() {
        maxShotsPerSecond = 8;
        coolDown = 0;
//        gameObject.forceThreshold = 700;
    }

    @Override
    public void update(GameObject gameObject, GameState gameState) {
//        if (gameObject.deformation >= 100) {
//            for (PhysicalConnector connector : gameObject.physicalConnectors) {
//                connector.strength = 0;
//            }
//        }
        if (gameObject.dataConnectors.size() > 0 && gameObject.dataConnectors.get(0).input > 0) {
            int shotsPerSecond;
            if (gameObject.dataConnectors.get(0).input > maxShotsPerSecond) {
                shotsPerSecond = maxShotsPerSecond;
            } else {
                shotsPerSecond = gameObject.dataConnectors.get(0).input;
            }

            if (coolDown <= 0) {
                Vector2 pos = new Vector2(0, gameObject.size.y / 2 + 1);

                GameObject shot = new GameObject(
                    (byte) 5,
                    gameState.createBox(
                        0.5f,
                        2,
                        gameObject.body.getWorldPoint(pos),
                        gameObject.body.getAngle(),
                        0.2f
                    ),
                    new Vector2(0.5f, 2),
                    new Particle(0.3f)
                );

                shot.body.setLinearVelocity(gameObject.body.getLinearVelocityFromLocalPoint(pos));

                Vector2 impulse = new Vector2(0, 16).rotateRad(gameObject.body.getAngle());
                shot.body.applyLinearImpulse(
                    impulse,
                    shot.body.getPosition(),
                    true
                );
                gameObject.body.applyLinearImpulse(new Vector2(impulse).scl(-1), gameObject.body.getPosition(), true);
                gameState.addQueue.add(shot);
                coolDown = 1f / shotsPerSecond;
            }
        }

        if (coolDown > 0) {
            coolDown -= Gdx.graphics.getDeltaTime();
        }
    }
}
