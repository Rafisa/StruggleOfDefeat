package ch.funroom.sod;

import ch.funroom.sod.game_state.Connection;
import ch.funroom.sod.game_state.GameObject;
import ch.funroom.sod.game_state.GameState;
import com.badlogic.gdx.math.Vector2;
import org.msgpack.core.MessagePacker;
import org.msgpack.value.ArrayValue;
import org.msgpack.value.Value;

import java.util.ArrayList;

public class SpawnGrid {
    int width;
    int height;
    ArrayList<SpawnPart> spawnParts;
    ArrayList<Pair<Integer, Boolean>> teamMembers;
    boolean ready;

    public SpawnGrid(int width, int height) {
        this.width = width;
        this.height = height;
        spawnParts = new ArrayList<>();
        teamMembers = new ArrayList<>();
    }

    public boolean update(ArrayList<Part> parts, ArrayList<Connection> connections) {
        for (Pair teamMember : teamMembers) {
            if ((int)teamMember.t < connections.size()) {
                Connection connection = connections.get((int)teamMember.t);
                for (IncomingMsg msg = connection.nextOutput(); msg != null; msg = connection.nextOutput()) {
                    try {
                        readMessage(parts, connections, msg, teamMember);
                    } catch (Exception e) {}
                }
            }
        }
        return ready;
    }

    public void sendSize(Connection connection) {
        connection.sendMessage((MessagePacker packer) -> {
            packer.packArrayHeader(2);
            packer.packInt(0);

            packer.packArrayHeader(1);
            packer.packArrayHeader(2);
            packer.packInt(width);
            packer.packInt(height);
        });

        for(SpawnPart spawnPart : spawnParts) {
            connection.sendMessage((MessagePacker packer) -> {
                Write.writeMsgHeader(packer, 2, 2);
                Write.writeSpawnGridHeader(packer, (byte) 0, 2);
                Write.writePart(packer, spawnPart);
            });
        }
    }

    public void sendToAll(ArrayList<Connection> connections, MessageWriter messageWriter) {
        for (Pair teamMember : teamMembers) {
            if ((int)teamMember.t < connections.size()) {
                connections.get((int)teamMember.t).sendMessage(messageWriter);
            }
        }
    }

    public void addPart(ArrayList<Part> parts, ArrayList<Connection> connections, SpawnPart spawnPart) {
        if (checkPart(parts, spawnPart)) {
            spawnParts.add(spawnPart);
            sendToAll(connections, (MessagePacker packer) -> {
                Write.writeMsgHeader(packer, 2, 1);
                Write.writeSpawnGridHeader(packer, (byte) 0, 1);
                Write.writePart(packer, spawnPart);
            });
        }
    }

    public void removePart(ArrayList<Connection> connections, int i) {
        if (spawnParts.size() > i) {
            spawnParts.remove(i);
            sendToAll(connections, (MessagePacker packer) -> {
                Write.writeMsgHeader(packer, 2, 2);
                Write.writeSpawnGridHeader(packer, (byte) 1, 2);
                packer.packInt(i);
            });
        }
    }

    public void changePart(
        ArrayList<Part> parts,
        ArrayList<Connection> connections,
        int i,
        SpawnPart spawnPart
    ) {
        if (spawnParts.size() > i && checkPart(parts, spawnPart)) {
            spawnParts.set(i, spawnPart);
            sendToAll(connections, (MessagePacker packer) -> {
                Write.writeMsgHeader(packer, 2, 2);
                Write.writeSpawnGridHeader(packer, (byte) 2, 3);
                packer.packInt(i);
                Write.writePart(packer, spawnPart);
            });
        }
    }

    public boolean checkPart(ArrayList<Part> parts, SpawnPart spawnPart) {
        Rect[] rects = spawnPart.rects(parts);
        for(SpawnPart spawnPart1 : spawnParts) {
            for (Rect rect0: spawnPart1.rects(parts)) {
                for (Rect rect1: rects) {
                    if (rect0.x0 < rect1.x1 && rect1.x0 < rect0.x1 &&
                        rect0.y0 < rect1.y1 && rect1.y0 < rect0.y1)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void readMessage(
        ArrayList<Part> parts,
        ArrayList<Connection> connections,
        IncomingMsg msg,
        Pair connection
    ) throws Exception {
        if (msg.type == 2) {
            ArrayValue value = msg.value.asArrayValue().get(0).asArrayValue();
            if (value.size() >= 1) {
                ArrayValue content = value.get(1).asArrayValue();
                switch (value.get(0).asIntegerValue().asInt()) {
                    case 0: {
                        SpawnPart spawnPart = Read.readSpawnPart(
                            content.get(0),
                            (int)connection.t
                        );
                        addPart(parts, connections, spawnPart);
                        break;
                    }
                    case 1:
                        removePart(connections, content.get(0).asIntegerValue().asInt());
                        break;
                    case 2:
                        SpawnPart spawnPart = Read.readSpawnPart(
                            content.get(1),
                            (int)connection.t
                        );

                        changePart(
                            parts,
                            connections,
                            content.get(0).asIntegerValue().asInt(),
                            spawnPart
                        );
                        break;
                    case 3:
                        connection.u = true;
                        boolean tmpReady = true;
                        for (Pair teamMember: teamMembers) {
                            tmpReady = tmpReady && (boolean)teamMember.u;
                        }
                        ready = tmpReady;
                }
            }
        }
    }

    public void load(ArrayList<Part> parts, GameState gameState, Vector2 pos, float ang) {
        for (SpawnPart spawnPart : spawnParts) {
            spawnPart.load(parts, gameState, pos, ang);
        }
    }

    public static class SpawnPart {
        public byte type;
        public boolean mirror;
        public int x, y;
        public int ang;
        public int connection;

        public SpawnPart(byte type, boolean mirror, int x, int y, int ang, int connection) {
            this.type = type;
            this.mirror = mirror;
            this.x = x;
            this.y = y;
            this.ang = ang;
            this.connection = connection;
        }

        public void load(ArrayList<Part> parts, GameState gameState, Vector2 gridPos, float gridAng) {
            Vector2 pos = new Vector2(x, y);
            if (parts.size() > (int)type) {
                Vector2 size = parts.get(type).size();
                if (ang % 2 == 1) {
                    float tmp = size.x;
                    size.x = size.y;
                    size.y = tmp;
                }
                pos.add(size.x / 2, size.y / 2);

                pos.rotateRad(gridAng);
                pos.add(gridPos);

                Float angRad = ((float)(ang % 4 * Math.PI / 2)) + gridAng;
                GameObject obj = GameObject.loadGameObject(parts, gameState, type, mirror, connection, pos, angRad);

                if (obj != null) {
                    gameState.gameObjects.add(obj);
                }
            }
        }

        // TODO: simplify collision by using Rectangle directly.
        public Rect[] rects(ArrayList<Part> parts) {
            ArrayList<Rectangle> rectangles;

            if (parts.size() > type) {
                rectangles = parts.get(type).rectangles;
            } else {
                rectangles = new ArrayList<>();
            }

            int width = 0, height = 0;
            for (Rectangle rectangle : rectangles) {
                int x = rectangle.x + rectangle.width;
                int y = rectangle.y + rectangle.height;
                if (x > width) width = x;
                if (y > height) height = y;
            }

            Rect[] rects = new Rect[rectangles.size()];
            for (int i = 0; i < rects.length; i++) {
                Rectangle rectangle = rectangles.get(i);
                int x0 = rectangle.x;
                int y0 = rectangle.y;
                int x1 = rectangle.x + rectangle.width;
                int y1 = rectangle.y + rectangle.height;

                if (ang % 4 > 1 ^ mirror) {
                    int tmp = x0;
                    x0 = width - x1;
                    x1 = width - tmp;
                }

                if ((ang + 1) % 4 > 1) {
                    int tmp = y0;
                    y0 = height - y1;
                    y1 = height - tmp;
                }

                if (ang % 2 == 1) {
                    int tmp = x0;
                    x0 = y0;
                    y0 = tmp;
                    tmp = x1;
                    x1 = y1;
                    y1 = tmp;
                }

                rects[i] = new Rect(x0 + x, y0 + y, x1 + x, y1 + y);
            }
            return rects;
        }
    }

    public static class Rect {
        int x0, y0, x1, y1;

        public Rect(int x0, int y0, int x1, int y1) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
        }
    }
}