package ch.funroom.sod;

import org.msgpack.core.MessagePacker;

import java.io.IOException;

public interface MessageWriter {
    void write(MessagePacker packer) throws IOException;
}
