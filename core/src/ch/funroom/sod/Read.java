package ch.funroom.sod;

import com.badlogic.gdx.math.Vector2;
import org.msgpack.value.ArrayValue;
import org.msgpack.value.Value;

import java.util.ArrayList;

public class Read {
    public static Pair<Integer, Integer> readGridVec(Value value) throws Exception {
        ArrayValue vecValue = value.asArrayValue();
        return new Pair(vecValue.get(0).asIntegerValue().asInt(), vecValue.get(1).asIntegerValue().asInt());
    }

    public static Vector2 readVector2(Value value) throws Exception {
        ArrayValue vecValue = value.asArrayValue();
        return new Vector2(vecValue.get(0).asFloatValue().toFloat(), vecValue.get(1).asFloatValue().toFloat());
    }

    public static Rectangle readRectangle(Value value) throws Exception {
        ArrayValue arrayValue = value.asArrayValue();
        Pair<Integer, Integer> pos = readGridVec(arrayValue.get(0));
        Pair<Integer, Integer> size = readGridVec(arrayValue.get(1));
        return new Rectangle(pos.t, pos.u, size.t, size.u);
    }

    public static Part readPart(Value value) throws Exception {
        Part part = new Part();
        part.rectangles = new ArrayList<>();
        part.dataConnectors = new ArrayList<>();
        part.physicalConnectors = new ArrayList<>();

        ArrayValue arrayValue = value.asArrayValue();
        part.behaviour = arrayValue.get(0).asIntegerValue().asByte();

        for (Value rectangle: arrayValue.get(1).asArrayValue()) {
            part.rectangles.add(readRectangle(rectangle));
        }
        for (Value dataConnector: arrayValue.get(2).asArrayValue()) {
            part.dataConnectors.add(readVector2(dataConnector));
        }
        for (Value physicalConnector: arrayValue.get(3).asArrayValue()) {
            Vector2 pos = readVector2(physicalConnector.asArrayValue().get(0));
            float strength = physicalConnector.asArrayValue().get(1).asFloatValue().toFloat();
            part.physicalConnectors.add(new Pair<>(pos, strength));
        }
        return part;
    }

    public static SpawnGrid.SpawnPart readSpawnPart(Value value, int connection) throws Exception {
        ArrayValue partValue = value.asArrayValue();
        Pair<Integer, Integer> pos = readGridVec(partValue.get(2));
        return new SpawnGrid.SpawnPart(
            partValue.get(0).asIntegerValue().asByte(),
            partValue.get(1).asBooleanValue().getBoolean(),
            pos.t,
            pos.u,
            partValue.get(3).asIntegerValue().asInt(),
            connection
        );
    }
}
