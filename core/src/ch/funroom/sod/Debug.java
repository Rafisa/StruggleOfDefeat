package ch.funroom.sod;

import ch.funroom.sod.game_state.GameObject;
import ch.funroom.sod.game_state.GameState;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

import java.util.Iterator;

/**
 * A class dedicated to debug behaviour. primarily Used to analyse the gameState
 */
public class Debug {
    private OrthographicCamera camera;
    static int moveSpeed = 800;
    static float zoomSpeed = 2f;
    private Box2DDebugRenderer debugRenderer = new Box2DDebugRenderer();
    private SpriteBatch batch = new SpriteBatch();
    private ShapeRenderer shapeRenderer;

    public Debug() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.zoom = 0.5f;
        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();
    }

    /**
     * handles wasd +- left and right click shipInputs to control the camera. Draws the debug renderer. Draws points for connector
     *
     * @param delta     delta time
     * @param gameState the gameState it should render.
     */
    public void render(float delta, GameState gameState) {

        // w = 51
        if (Gdx.input.isKeyPressed(51)) {
            camera.position.y += ((moveSpeed * delta) * camera.zoom);
        }
        //A = 29
        if (Gdx.input.isKeyPressed(29)) {
            camera.position.x -= ((moveSpeed * delta) * camera.zoom);
        }
        //S = 47
        if (Gdx.input.isKeyPressed(47)) {
            camera.position.y -= ((moveSpeed * delta) * camera.zoom);
        }
        //D = 32
        if (Gdx.input.isKeyPressed(32)) {
            camera.position.x += ((moveSpeed * delta) * camera.zoom);
        }
        //left = 0
        if (Gdx.input.isButtonPressed(0)) {
            camera.zoom -= ((zoomSpeed * delta) * camera.zoom);
        }
        //right = 1
        if (Gdx.input.isButtonPressed(1)) {
            camera.zoom += ((zoomSpeed * delta) * camera.zoom);
        }
        //81 = +
        if (Gdx.input.isKeyJustPressed(81)) {
            zoomSpeed *= 1.5;
            moveSpeed *= 1.5;
        }
        //69 = -
        if (Gdx.input.isKeyJustPressed(69)) {
            zoomSpeed *= 0.5;
            moveSpeed *= 0.5;
        }

        //clears screen
        Gdx.gl20.glClearColor(0, 0, 0, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //update method of camera
        camera.update();

        batch.setProjectionMatrix(camera.combined);
        //renders debug lines
        debugRenderer.render(gameState.world, batch.getProjectionMatrix());
        //renders connector points
        renderPoints(gameState);
    }

    public void resize(int width, int height) {
        Vector3 pos = new Vector3(camera.position);
        float zoom = camera.zoom;
        camera.setToOrtho(false, width, height);
        camera.position.set(pos);
        camera.zoom = zoom;
        camera.update();
    }

    /**
     * draws a hexagon on each location of a connector
     *
     * @param gameState the gameState for which it should draw
     */
    private void renderPoints(GameState gameState) {
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        //iteration over all gameObjects
        Iterator<GameObject> iter = gameState.gameObjects.iterator();
        while (iter.hasNext()) {
            GameObject next = iter.next();
            shapeRenderer.setColor(Color.ORANGE);
            shapeRenderer.circle(next.body.getPosition().x, next.body.getPosition().y, 0.05f, 6);
            shapeRenderer.setColor(Color.BLUE);
            //iteration over all physicalConnectors of a body
            for (int i = 0; i < next.dataConnectors.size(); i++) {
                Vector2 worldPos = next.body.getWorldPoint(next.dataConnectors.get(i).pos);
                //draws a hexagon at location of connector.
                shapeRenderer.circle(worldPos.x, worldPos.y, 0.05f, 6);
            }
//            shapeRenderer.setColor(Color.WHITE);
//            for (int i = 0; i < next.physicalConnectors.size(); i++) {
//                Vector2 worldPos = next.body.getWorldPoint(next.physicalConnectors.get(i).pos);
//                //draws a hexagon at location of connector.
//                shapeRenderer.circle(worldPos.x, worldPos.y, 0.05f, 6);
//            }
        }
        shapeRenderer.end();
    }
}
