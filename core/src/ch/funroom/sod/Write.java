package ch.funroom.sod;

import org.msgpack.core.MessagePacker;

import java.io.IOException;

public class Write {
    public static void writeMsgHeader(MessagePacker packer, int msgType, int len) throws IOException {
        packer.packArrayHeader(2);
        packer.packInt(msgType);
        packer.packArrayHeader(len);
    }

    public static void writeSpawnGridHeader(MessagePacker packer, byte msgType, int len) throws IOException {
        packer.packArrayHeader(2);
        packer.packByte(msgType);
        packer.packArrayHeader(len);
    }

    public static void writePart(MessagePacker packer, SpawnGrid.SpawnPart spawnPart) throws IOException {
        packer.packArrayHeader(4);
        packer.packInt(spawnPart.type);
        packer.packBoolean(spawnPart.mirror);
        packer.packArrayHeader(2);
        packer.packInt(spawnPart.x);
        packer.packInt(spawnPart.y);
        packer.packInt(spawnPart.ang);
    }
}
