package ch.funroom.sod;

import org.msgpack.value.Value;

public class IncomingMsg {
    public int type;
    public Value value;
}
