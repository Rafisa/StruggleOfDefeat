package ch.funroom.sod.desktop;

import ch.funroom.sod.StruggleOfDefeat;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;

/**
 * this is the main class of the project. it initiates the LwjglApplication with the parameter config witch gets
 * generated here
 */
public class DesktopLauncher {
    /**
     * creates the LwjglApplicationConfiguration and sets some configurations
     *
     * @param arg unused
     */
    public static void main(String[] arg) {
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setTitle("Struggle Of Defeat");
        config.setWindowedMode(1280, 720);
        config.useVsync(true);
        config.setInitialBackgroundColor(new Color(0.078431372549f, 0.141176470588f, 0.372549019608f, 1));
        new Lwjgl3Application(new StruggleOfDefeat(), config);
        System.exit(0);
    }
}
